// for terminal/gitbash
	//See `npm help init` for definitive documentation on these fields and exactly what they do.
	//- npm init =  creating a package.json
	// npm init -y = shortcut for npm init
	// npm install express = install express to terminal
	// touch .gitignore = to ignore the node_modules in pushing to gitlab

// This code will help us access of express module/package
	// A "module" is a software component or part of a program that contains one or more routines
//it also allows us to access methods and functions that we will use to easily create an app/server
// We store our express module to variable so we could easily access its keywords, function, and methods.
const express = require("express");

// this code creates an application using express / a.k.a express application
	// 'App' is our server
const app = express();

// For our application server to run, we need a port to .listen() to
const port = 3000;

// For our application coud read json data
app.use(express.json());

// Allows your app to read data from forms
// By default, information received from the url can only be received as string or an array
// By applying the option of "extended:true" this allows us to receive information in other data types, such as an object which we will use throughout our application.
app.use(express.urlencoded({extended:true}));

// This route expects to receive a GET request at the URI/endpoint "/hello"
app.get("/hello", (request, response)=>{

	// This is the response that we will expect to receive if the GET method is successful with the right endpoint is successful
	response.send("GET method success. \nHello from /hello endpoint");



});

app.post("/hello", (request, response)=> {

	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
});

let users = [];

app.post("/singup", (request, response) => {
	if(request.body.username !== "" && request.body.password !== ""){
		users.push(request.body);
		console.log(users);
		response.send (`User ${request.body.username} successfully registered`);

		// Sending another response will result an error, only the first response will display
		// response.send("Welcome admin");
	}
	else{
		response.send("Please input BOTH username and password");
	}

});


// Syntax: 
/*
app.httpMethod("/endpoint", (request, response) => {
	//code block
});
*/

// We use put method to update a document
app.put("/change-password", (request, response) => {

	// We create a variable where we will store our response
	let message;

	for(let i=0; i < users.length; i++){
		// The purpose of our loop is to check our per element in users array if it matchs our request.body.username
		// We check if the user is existing in our users array
		if(request.body.username == users[i].username){

			// update an element object's password based on the input on the body 
			users[i].password = request.body.password;

			// We reassign what message we would like to receive as response
			message = `User ${request.body.username}'s password has been updated`;
			break;
		} 
		else{

			// if there is no match we would like to receive a message that user does not exist
			message = "User does not exist."
		};
	}
	// we display our updated array
	console.log(users);

	// We display our response based if there is a match (successfuly updated the password) or if there is no match (user does not exist)
	response.send(message);
});


// 1
// Create a route that expects GET request at the URI/endpoint "/home"
// Response that will be received should be "Welcome to the homepage"
// Code below...

app.get("/home", (request, response)=> {

	response.send("Welcome to the homepage");
});

// 2
// Create a route that expects GET request at the URI/endpoint "/users"
// Response that will be received is a collection of users

app.get("/users", (request, response)=> {

	response.send(users);
});


// 3
// Create a route that expects DELETE request at the URI/endpoint "/delete-user"
// The program should check if the user is existing before deleting
// Response that will be received is the updated collection of users

app.delete("/delete-user", (request, response) => {
	let message;

    for(let i = 0; i < users.length; i++){
        if(request.body.username == users[i].username){
            users.splice(i, 1);
            message = "Successfully deleted"
        }
    }
    console.log(users);
    response.send(message);
});
// 4
// Save all the successful request tabs and export the collection
// Update your GitLab repo and link it to Boodle



// Tells our server/application to listen to the port
// If the port is accessed, we can run the server
// Returns a message to confirm that the server is running in the terminal
app.listen(port, () => console.log(`Server running at port ${port}`));